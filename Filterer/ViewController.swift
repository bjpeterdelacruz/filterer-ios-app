//
//  ViewController.swift
//  Filterer
//
//  Created by Jack on 2015-09-22.
//  Copyright © 2015 UofT. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var secondaryMenu: UIView!
    @IBOutlet var bottomMenu: UIView!
    
    @IBOutlet var filterButton: UIButton!
    
    @IBOutlet var compareButton: UIButton!
    
    @IBOutlet var intensitySlider: UISlider!
    
    @IBOutlet var originalBtn: UIButton!
    @IBOutlet var redBtn: UIButton!
    @IBOutlet var greenBtn: UIButton!
    @IBOutlet var blueBtn: UIButton!
    @IBOutlet var yellowBtn: UIButton!
    @IBOutlet var grayBtn: UIButton!
    @IBOutlet var brightnessBtn: UIButton!
    @IBOutlet var blackAndWhiteBtn: UIButton!
    
    var originalImage: UIImage?
    var filteredImage: UIImage?
    var currentFilter: FilterEnum?
    
    @IBAction func onValueChanged(sender: UISlider) {
        if filteredImage != nil {
            if let color = currentFilter as? Color {
                switch color {
                case .Red:
                    onRed(nil)
                case .Green:
                    onGreen(nil)
                case .Blue:
                    onBlue(nil)
                case .Yellow:
                    onYellow(nil)
                case .Gray:
                    onGray(nil)
                }
            } else if let type = currentFilter as? FilterType {
                switch type {
                case .Brightness:
                    onBrightness(nil)
                case .BlackAndWhite:
                    onBlackAndWhite(nil)
                }
            }
        }
    }
    
    @IBAction func onOriginal(sender: UIImage?) {
        if filteredImage != nil {
            setImage(originalImage!)
            filteredImage = nil
            compareButton.enabled = false
            intensitySlider.enabled = false
            intensitySlider.alpha = 0
        }
    }
    
    @IBAction func onRed(sender: UIButton?) {
        let rgbaImage = RGBAImage(image: originalImage!)!
        let red = RemoveColor(colorsToRemove: [Color.Green, Color.Blue])
        var intensity = Int(intensitySlider.value)
        // sender is not nil if user pressed button to apply filter
        // sender is nil if user is using slider to change filter intensity
        if sender != nil {
            intensity = 0
            updateSliderMinMaxValues(-100, max: 100)
            intensitySlider.value = 0
        }
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        // apply filter in background thread, then update UI once finished
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            // similar to Java's synchronized block
            objc_sync_enter(self)
            red.applyFilter(rgbaImage, colorToAdjustIntensity: Color.Red, intensity: intensity)
            objc_sync_exit(self)
            dispatch_async(dispatch_get_main_queue()) {
                self.updateView(rgbaImage.toUIImage()!)
                self.currentFilter = Color.Red
            }
        }
    }
    
    @IBAction func onGreen(sender: UIButton?) {
        let rgbaImage = RGBAImage(image: originalImage!)!
        let green = RemoveColor(colorsToRemove: [Color.Red, Color.Blue])
        var intensity = Int(intensitySlider.value)
        if sender != nil {
            intensity = 0
            updateSliderMinMaxValues(-100, max: 100)
            intensitySlider.value = 0
        }
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            objc_sync_enter(self)
            green.applyFilter(rgbaImage, colorToAdjustIntensity: Color.Green, intensity: intensity)
            objc_sync_exit(self)
            dispatch_async(dispatch_get_main_queue()) {
                self.updateView(rgbaImage.toUIImage()!)
                self.currentFilter = Color.Green
            }
        }
    }
    
    @IBAction func onBlue(sender: UIButton?) {
        let rgbaImage = RGBAImage(image: originalImage!)!
        let blue = RemoveColor(colorsToRemove: [Color.Red, Color.Green])
        var intensity = Int(intensitySlider.value)
        if sender != nil {
            intensity = 0
            updateSliderMinMaxValues(-100, max: 100)
            intensitySlider.value = 0
        }
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            objc_sync_enter(self)
            blue.applyFilter(rgbaImage, colorToAdjustIntensity: Color.Blue, intensity: intensity)
            objc_sync_exit(self)
            dispatch_async(dispatch_get_main_queue()) {
                self.updateView(rgbaImage.toUIImage()!)
                self.currentFilter = Color.Blue
            }
        }
    }
    
    @IBAction func onYellow(sender: UIButton?) {
        let rgbaImage = RGBAImage(image: originalImage!)!
        let yellow = RemoveColor(colorsToRemove: [Color.Blue])
        var intensity = Int(intensitySlider.value)
        if sender != nil {
            intensity = 0
            updateSliderMinMaxValues(-100, max: 100)
            intensitySlider.value = 0
        }
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            objc_sync_enter(self)
            yellow.applyFilter(rgbaImage, colorToAdjustIntensity: Color.Yellow, intensity: intensity)
            objc_sync_exit(self)
            dispatch_async(dispatch_get_main_queue()) {
                self.updateView(rgbaImage.toUIImage()!)
                self.currentFilter = Color.Yellow
            }
        }
    }
    
    @IBAction func onGray(sender: UIButton?) {
        let rgbaImage = RGBAImage(image: originalImage!)!
        let gray = Grayscale()
        var intensity = Int(intensitySlider.value)
        if sender != nil {
            intensity = 8
            updateSliderMinMaxValues(3, max: 12)
            intensitySlider.value = 15 / 2
        }
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            objc_sync_enter(self)
            try! gray.applyFilter(rgbaImage, colorToAdjustIntensity: nil, intensity: intensity)
            objc_sync_exit(self)
            dispatch_async(dispatch_get_main_queue()) {
                self.updateView(rgbaImage.toUIImage()!)
                self.currentFilter = Color.Gray
            }
        }
    }
    
    @IBAction func onBrightness(sender: UIButton?) {
        let rgbaImage = RGBAImage(image: originalImage!)!
        let brightness = Brightness()
        var intensity = Int(intensitySlider.value)
        if sender != nil {
            intensity = 80
            updateSliderMinMaxValues(-100, max: 100)
            intensitySlider.value = 80
        }
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            objc_sync_enter(self)
            brightness.applyFilter(rgbaImage, colorToAdjustIntensity: nil, intensity: intensity)
            objc_sync_exit(self)
            dispatch_async(dispatch_get_main_queue()) {
                self.updateView(rgbaImage.toUIImage()!)
                self.currentFilter = FilterType.Brightness
            }
        }
    }
    
    @IBAction func onBlackAndWhite(sender: UIButton?) {
        let rgbaImage = RGBAImage(image: originalImage!)!
        let blackAndWhite = BlackAndWhite()
        var intensity = Int(intensitySlider.value)
        if intensity < 100 {
            intensity = 300
            updateSliderMinMaxValues(100, max: 500)
            intensitySlider.value = Float(intensity)
        }
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            objc_sync_enter(self)
            blackAndWhite.applyFilter(rgbaImage, colorToAdjustIntensity: nil, intensity: intensity)
            objc_sync_exit(self)
            dispatch_async(dispatch_get_main_queue()) {
                self.updateView(rgbaImage.toUIImage()!)
                self.currentFilter = FilterType.BlackAndWhite
            }
        }
    }
    
    private func updateSliderMinMaxValues(min: Float, max: Float) {
        intensitySlider.minimumValue = min
        intensitySlider.maximumValue = max
    }
    
    private func updateView(image: UIImage) {
        setImage(image)
        filteredImage = imageView.image
        compareButton.enabled = true
        intensitySlider.enabled = true
        if intensitySlider.alpha == 0 {
            UIView.animateWithDuration(1) {
                self.intensitySlider.alpha = 1.0
            }
        }
    }
    
    private func setImage(image: UIImage) {
        UIView.transitionWithView(imageView,
            duration:1,
            options: UIViewAnimationOptions.TransitionCrossDissolve,
            animations: { self.imageView.image = image },
            completion: nil)
    }
    
    @IBAction func onCompare(sender: UIButton) {
        switchImage()
    }
    
    private func switchImage() {
        if let img = filteredImage {
            if imageView.image === img {
                setImage(originalImage!)
                intensitySlider.enabled = false
            } else {
                setImage(img)
                intensitySlider.enabled = true
            }
        }
    }
    
    func onTap(sender: UITapGestureRecognizer) {
        switchImage()
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.switchImage()
        }
    }
    
    func onLongPress(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.Began {
            switchImage()
        } else if sender.state == UIGestureRecognizerState.Ended {
            switchImage()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        secondaryMenu.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        secondaryMenu.translatesAutoresizingMaskIntoConstraints = false
        // Save original image so we can toggle between original and filtered images
        originalImage = imageView.image
        // Disable Compare button until filter has been applied to image
        compareButton.enabled = false
        imageView.userInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: Selector("onTap:"))
        imageView.addGestureRecognizer(tap)
        let longPress = UILongPressGestureRecognizer(target: self, action: Selector("onLongPress:"))
        imageView.addGestureRecognizer(longPress)
        let original = UIImage(named: "original")
        originalBtn.setBackgroundImage(original, forState: .Normal)
        let red = UIImage(named: "red")
        redBtn.setBackgroundImage(red, forState: .Normal)
        let green = UIImage(named: "green")
        greenBtn.setBackgroundImage(green, forState: .Normal)
        let blue = UIImage(named: "blue")
        blueBtn.setBackgroundImage(blue, forState: .Normal)
        let yellow = UIImage(named: "yellow")
        yellowBtn.setBackgroundImage(yellow, forState: .Normal)
        let gray = UIImage(named: "gray")
        grayBtn.setBackgroundImage(gray, forState: .Normal)
        let brightness = UIImage(named: "brightness")
        brightnessBtn.setBackgroundImage(brightness, forState: .Normal)
        let blackAndWhite = UIImage(named: "bw")
        blackAndWhiteBtn.setBackgroundImage(blackAndWhite, forState: .Normal)
        intensitySlider.continuous = false
        intensitySlider.alpha = 0
    }

    // MARK: Share
    @IBAction func onShare(sender: AnyObject) {
        let activityController = UIActivityViewController(activityItems: ["Check out our really cool app", imageView.image!], applicationActivities: nil)
        presentViewController(activityController, animated: true, completion: nil)
    }
    
    // MARK: New Photo
    @IBAction func onNewPhoto(sender: AnyObject) {
        let actionSheet = UIAlertController(title: "New Photo", message: nil, preferredStyle: .ActionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { action in
            self.showCamera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Album", style: .Default, handler: { action in
            self.showAlbum()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func showCamera() {
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = .Camera
        
        presentViewController(cameraPicker, animated: true, completion: nil)
    }
    
    func showAlbum() {
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = .PhotoLibrary
        
        presentViewController(cameraPicker, animated: true, completion: nil)
    }
    
    // MARK: UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        dismissViewControllerAnimated(true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            originalImage = image
            onOriginal(nil)
            imageView.image = image
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: Filter Menu
    @IBAction func onFilter(sender: UIButton) {
        if (sender.selected) {
            hideSecondaryMenu()
            sender.selected = false
        } else {
            showSecondaryMenu()
            sender.selected = true
        }
    }
    
    func showSecondaryMenu() {
        view.addSubview(secondaryMenu)
        
        let bottomConstraint = secondaryMenu.bottomAnchor.constraintEqualToAnchor(bottomMenu.topAnchor)
        let leftConstraint = secondaryMenu.leftAnchor.constraintEqualToAnchor(view.leftAnchor)
        let rightConstraint = secondaryMenu.rightAnchor.constraintEqualToAnchor(view.rightAnchor)
        
        let heightConstraint = secondaryMenu.heightAnchor.constraintEqualToConstant(88)
        
        NSLayoutConstraint.activateConstraints([bottomConstraint, leftConstraint, rightConstraint, heightConstraint])
        
        view.layoutIfNeeded()
        
        self.secondaryMenu.alpha = 0
        UIView.animateWithDuration(0.4) {
            self.secondaryMenu.alpha = 1.0
        }
    }

    func hideSecondaryMenu() {
        UIView.animateWithDuration(0.4, animations: {
            self.secondaryMenu.alpha = 0
            }) { completed in
                if completed == true {
                    self.secondaryMenu.removeFromSuperview()
                }
        }
    }

}

