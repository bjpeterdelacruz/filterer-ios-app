import UIKit

protocol Filter {
    func applyFilter(image: RGBAImage, colorToAdjustIntensity: Color?, intensity: Int) throws -> Void
}

enum Error: ErrorType {
    case DivideByZero
}

class Grayscale: Filter {
    
    func applyFilter(let image: RGBAImage, colorToAdjustIntensity: Color?, intensity: Int) throws {
        guard intensity != 0 else {
            throw Error.DivideByZero
        }
        for y in 0..<image.height {
            for x in 0..<image.width {
                let index = y * image.width + x
                
                var pixel = image.pixels[index]
                
                let value = UInt8((Int(pixel.red) + Int(pixel.green) + Int(pixel.blue)) / Int(intensity))
                pixel.red = value
                pixel.green = value
                pixel.blue = value
                
                image.pixels[index] = pixel
            }
        }
    }
}

protocol FilterEnum {
    
}

enum Color: FilterEnum {
    case Red
    case Green
    case Blue
    case Yellow
    case Gray
}

enum FilterType: FilterEnum {
    case Brightness
    case BlackAndWhite
}

class RemoveColor: Filter {

    let colorsToRemove:[Color]

    init(colorsToRemove: [Color]) {
        self.colorsToRemove = colorsToRemove
    }
    
    func applyFilter(let image: RGBAImage, colorToAdjustIntensity: Color?, intensity: Int) {
        for y in 0..<image.height {
            for x in 0..<image.width {
                let index = y * image.width + x
                
                var pixel = image.pixels[index]
                
                for color in colorsToRemove {
                    switch color {
                    case .Red:
                        pixel.red = 0
                    case .Green:
                        pixel.green = 0
                    case .Blue:
                        pixel.blue = 0
                    case .Yellow:
                        fallthrough
                    case .Gray:
                        break
                    }
                }
                
                switch colorToAdjustIntensity! {
                case .Red:
                    pixel.red = UInt8(max(0, min(255, Int(pixel.red) + Int(intensity))))
                case .Green:
                    pixel.green = UInt8(max(0, min(255, Int(pixel.green) + Int(intensity))))
                case .Blue:
                    pixel.blue = UInt8(max(0, min(255, Int(pixel.blue) + Int(intensity))))
                case .Yellow:
                    pixel.red = UInt8(max(0, min(255, Int(pixel.red) + Int(intensity))))
                    pixel.green = UInt8(max(0, min(255, Int(pixel.green) + Int(intensity))))
                case .Gray:
                    break
                }
                
                image.pixels[index] = pixel
            }
        }
    }
}

class Brightness: Filter {
    
    func applyFilter(let image: RGBAImage, colorToAdjustIntensity: Color?, intensity: Int) {
        for y in 0..<image.height {
            for x in 0..<image.width {
                let index = y * image.width + x
                
                var pixel = image.pixels[index]
                
                pixel.red = UInt8(max(0, min(255, Int(intensity) + Int(pixel.red))))
                pixel.green = UInt8(max(0, min(255, Int(intensity) + Int(pixel.green))))
                pixel.blue = UInt8(max(0, min(255, Int(intensity) + Int(pixel.blue))))
                
                image.pixels[index] = pixel
            }
        }
    }
}

class BlackAndWhite: Filter {
    
    func applyFilter(let image: RGBAImage, colorToAdjustIntensity: Color?, intensity: Int) {
        for y in 0..<image.height {
            for x in 0..<image.width {
                let index = y * image.width + x
                
                var pixel = image.pixels[index]
                
                let red = pow(Double(255 - Int(pixel.red)), 2)
                let green = pow(Double(255 - Int(pixel.green)), 2)
                let blue = pow(Double(255 - Int(pixel.blue)), 2)
                let result = Int(sqrt(red + green + blue))
                
                if result > intensity {
                    pixel.red = 255
                    pixel.green = 255
                    pixel.blue = 255
                } else {
                    pixel.red = 0
                    pixel.green = 0
                    pixel.blue = 0
                }
                
                image.pixels[index] = pixel
            }
        }
    }
}
